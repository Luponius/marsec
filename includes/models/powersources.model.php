<?php

class Powersources{
	
	// The find static method returns an array
	// with Powersource objects from the database.
	
	public static function find($arr){
		global $db;
		
		if($arr['category'])
		{
			$st = $db->prepare("SELECT * FROM powersources WHERE po_caId = :category");
		}
		else
		{
			throw new Exception("Unsupported property!");
		}
		
		$st->execute($arr);
		
		return $st->fetchAll(PDO::FETCH_CLASS, "Powersources");
	}
}

?>