<?php render('_header',array('title'=>$title))?>

<p>Welcome! Try browsing this site using different browser resolutions.</p>

<ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="b">
    <li data-role="list-divider">Choose a product category</li>
    <?php render($content) ?>
</ul>

<?php render('_footer')?>