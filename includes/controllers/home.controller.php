<?php

/* This controller renders the home page */

class HomeController{
	public function handleRequest(){
		
		// Select all the categories from (includes/models/category.model.php)
		$content = Category::find();
		
		// includes/helpers.php
		render('home',array(
			'title'		=> 'Welcome to test page',
			'content'	=> $content
		));
	}
}

?>