<?php

/*
	This is the index file of our simple website.
	It routes requets to the appropriate controllers
*/

require_once "includes/main.php";

try {
	// If URL contains a category start it (includes/controllers/category.controller.php)
	if($_GET['category']){
		$c = new CategoryController();
	}
	// Otherwise start a basic page (includes/controllers/home.controller.php)
	else if(empty($_GET)){
		$c = new HomeController();
	}
	// If no success provide wrong page error
	else throw new Exception('Wrong page!');
	
	// run handleRequest on created variable residing in either Category or Home controller.
	$c->handleRequest();
}
// Handle exceptions using (includes/helpers.php --> render())
catch(Exception $e) {
	// Display the error page using the "render()" helper function:
	render('error',array('message'=>$e->getMessage()));
}

?>