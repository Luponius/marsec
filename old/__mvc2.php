<?php

// Create Model
class Model {
	public $text;
  	
  	// Construct a test `text` string
	public function __construct() {
		$this->text = 'Hello world!';
   }        
}

// Create Controller
class Controller {
	private $model;

	// Save locally model object given
	public function __construct(Model $model) {
		$this->model = $model;
	}

	public function textClicked() {
		$this->model->text = 'Text Updated!';
	}
}

// Create a View
class View {
	private $model;
	private $route;
   
   // Save locally controller/model object given
	public function __construct($route, Model $model) {
		$this->route = $route;
		$this->model = $model;
    }
   
   // Output function providing back display
   public function output() {
   	// Output text String as Header 1
		return '<a href="mvc2.php?route=' . $this->route . '?action=textclicked">' . $this->model->text .'</a>';
	}
    
}

class FrontController {
	private $controller;
	private $view;

	public function __construct(Router $router, $routeName, $action = null) {
		$route = $router->getRoute($routeName);
		$modelName = $route->model;
		$controllerName = $route->controller;
		$viewName = $route->view;

		$model = new $modelName;
		$this->controller = new $controllerName($model);
		$this->view = new $viewName ($routeName, $model);

		if (!empty($action))
			$this->controller->{$action}();
	}

	public function output() {
		// This allows for some consistent layout generation code
		$header = '<h1>Hello World Example</h1>';
		return $header . '<div>' . $this->view->output() . '</div>';
	}
}

class Router {
	private $table = array();

	public function __construct() {
		$this->table['controller'] = new Route('Model', 'View' , 'Controller');
	}

	public function getRoute($route) {
		$route = strtolower($route);

		if (!isset($this->table[$route]))
			return ($this->table['controller']);
		return $this->table[$route];
	}
}

class Route {
	public $model;
	public $view;
	public $controller;

	public function __construct($model, $view, $controller){
		$this->model = $model;
		$this->view = $view;
		$this->controller = $controller;
	}
}

$frontController = new FrontController(new Router, $_GET['route'], 
						isset($_GET['action']) ? $_GET['action'] : null);
echo $frontController->output();
?>