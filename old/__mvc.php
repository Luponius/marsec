<?php

// Create Model
class Model {
	public $text;
  	
  	// Construct a test `text` string
	public function __construct() {
		$this->text = 'Hello world!';
   }        
}

// Create Controller
class Controller {
	private $model;

	// Save locally model object given
	public function __construct(Model $model) {
		$this->model = $model;
	}

	public function textClicked() {
		$this->model->text = 'Text Updated!';
	}
}

// Create a View
class View {
	private $model;
	private $controller;
   
   // Save locally controller/model object given
	public function __construct(Controller $controller, Model $model) {
		$this->controller = $controller;
		$this->model = $model;
    }
   
   // Output function providing back display
   public function output() {
   	// Output text String as Header 1
		return '<a href="mvc.php?action=textclicked">' . $this->model->text .'</h1>';
	}
    
}





//initiate the triad
$model = new Model();
//It is important that the controller and the view share the model
$controller = new Controller($model);
$view = new View($controller, $model);

// If `action` paramater is set, call the action handler given
if (isset($_GET['action']))
	$controller->{$_GET['action']}();

echo $view->output();
?>