<?php

class Model {
  public $dbh;
  public $text;
  
  public function __construct($dbh) {
      $this->text = 'Hello world!';
      $this->dbh = $dbh;
  }

  // Method used to find data from SQL table "battery"
  public static function find($dbh, $arr = array()) {

    // Prepare statement for execution
    $st = $dbh->prepare("SELECT * FROM battery");

    // Execute prepared statement
    $st->execute($arr);

    // Return results as a Model object, with columns of
    // each row listed as a public property of the object
    return $st->fetchAll(PDO::FETCH_CLASS, "Model");
  }
}


class View {
  private $model;
  private $controller;
  
  public function __construct(Controller $controller, Model $model) {
    $this->controller = $controller;
    $this->model = $model;
  }
  
  public function output() {
    return '<h1>' . $this->model->text .'</h1>';
  }
}

class Controller {
  private $model;

  public function __construct($dbh, Model $model) {
    $this->model = $model;
    $content = Model::find($dbh);
    print_r($content);
  }
}

$user = 'root';
$pass = '';

try {
  $dbh = new PDO('mysql:host=localhost;dbname=marsec', $user, $pass);
} 
catch (PDOException $e) {
  print "Error!: " . $e->getMessage() . "<br/>";
  die();
}

//initiate the triad
$model = new Model($dbh);
//It is important that the controller and the view share the model
$controller = new Controller($dbh, $model);
$view = new View($controller, $model);
echo $view->output();

// Clear the connection once done
$dbh = null;
?>